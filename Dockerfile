FROM debian:9 AS builder

RUN apt update && apt install -y wget  gcc make libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev openssl git
RUN wget http://nginx.org/download/nginx-1.19.3.tar.gz && mkdir -p /usr/local/nginx/sbin && \
tar -zxvf nginx-1.19.3.tar.gz && cd /usr/local/src && wget http://luajit.org/download/LuaJIT-2.1.0-beta2.tar.gz && tar zxf LuaJIT-2.1.0-beta2.tar.gz && \
cd LuaJIT-2.1.0-beta2 && make PREFIX=/usr/local/luajit && make install PREFIX=/usr/local/luajit && cd /usr/local/src && \
git clone https://github.com/openresty/lua-resty-core  && cd lua-resty-core/ && make && make install && cd /usr/local/src && \
git clone https://github.com/openresty/lua-resty-lrucache.git && cd lua-resty-lrucache/ && make install && cd /usr/local/src && \
git clone https://github.com/openresty/lua-nginx-module.git && export LUAJIT_LIB=/usr/local/luajit/lib/ && \
export LUAJIT_INC=/usr/local/luajit/include/luajit-2.1 && cd /usr/local/src && \
wget https://github.com/vision5/ngx_devel_kit/archive/refs/tags/v0.3.1.tar.gz && tar xvf v0.3.1.tar.gz && cd /nginx-1.19.3 && \
./configure  \
--sbin-path=/usr/local/nginx/sbin \
--conf-path=/etc/nginx/nginx.conf \
--error-log-path=/var/log/nginx/error.log \
--http-log-path=/var/log/nginx/access.log \
--pid-path=/var/run/nginx.pid  \
--with-pcre \
--with-http_ssl_module \
--with-http_stub_status_module \
--with-http_gzip_static_module \
--with-http_realip_module  \
--with-pcre-jit \
--with-ld-opt='-ljemalloc' \
--with-ld-opt="-Wl,-rpath,/usr/local/luajit/lib" \
--add-module=/usr/local/src/lua-nginx-module/ \
--add-module=/usr/local/src/ngx_devel_kit-0.3.1/ && \
make && make install

FROM debian:9
COPY --from=builder /usr/local/nginx/html/ /usr/local/nginx/html
COPY --from=builder /etc/nginx/ /etc/nginx
COPY --from=builder /usr/lib/x86_64-linux-gnu/ /usr/lib/x86_64-linux-gnu/
COPY --from=builder /usr/local/luajit/ /usr/local/luajit/
COPY --from=builder /usr/local/src/ /usr/local/src/
COPY --from=builder /usr/local/lib/lua/ /usr/local/lib/lua
COPY --from=builder /usr/local/src/ /usr/local/src/
WORKDIR /usr/local/nginx/sbin/
COPY --from=builder /usr/local/nginx/sbin/nginx .
RUN mkdir -p /var/log/nginx && chmod +x nginx && useradd --no-create-home nginx
CMD ["./nginx", "-g", "daemon off;"]

